<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use App\Model\Factory\OfferFilterFactory;

class OfferFilterFactoryTest extends TestCase
{
    private $factory;

    public function setUp(): void
    {
        $factory = new OfferFilterFactory();
        $this->factory = $factory;
    }

    public function testCreatesAppropriateTypedObjectFromValidOptions()
    {
        $filter = $this->factory->createFilter([null, OfferFilterFactory::FILTER_NAME__VENDOR_OFFERS_FILTER, 1]);
        $this->assertInstanceOf(\App\Filter\OfferVendorFilter::class, $filter);

        $filter = $this->factory->createFilter([null, OfferFilterFactory::FILTER_NAME__PRICE_FILTER, '10.00', '100.00']);
        $this->assertInstanceOf(\App\Filter\OfferPriceFilter::class, $filter);

        $filter = $this->factory->createFilter([null, OfferFilterFactory::FILTER_NAME__DATE_FILTER, '2020-05-01T10:10', '2020-06-01T10:20']);
        $this->assertInstanceOf(\App\Filter\OfferDateFilter::class, $filter);
    }

    public function testThrowsExceptionOnInvalidFilterArgumentType()
    {
        $this->expectException(\TypeError::class);

        $this->factory->createFilter('definitely wrong filter type 11234d');
    }

    public function testThrowsExceptionOnInvalidFilterArgumentOptions()
    {
        $this->expectException(\Exception::class);

        $this->factory->createFilter([null, 'definitely wrong filter name 11234d', 10, 5, 6]);
    }
}