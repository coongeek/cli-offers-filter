<?php
declare(strict_types=1);
ini_set('xdebug.var_display_max_children', "-1");
ini_set('xdebug.var_display_max_data', "-1");
ini_set('xdebug.var_display_max_depth', "-1");
require __DIR__ . '/vendor/autoload.php';

use App\Model\Adapter\OutputAdapter;
use App\Model\Factory\OfferFilterFactory;
use App\Model\Reader\StreamReader;
use Symfony\Component\Cache\Adapter\MemcachedAdapter;
use App\Iterator\FilterIterator\OfferCollectionFilterIterator;
use App\Filter\OfferInStockFilter;
/**
 * Feci quod potui faciant meliora potentes
 */
$outputAdapter = new OutputAdapter();
try {
    $memcached = new Memcached('offer_pool');
    if (! count($memcached->getServerList())) {
        $memcached->addServer('127.0.0.1', 11211);
    }
    $memcachedAdapter = new MemcachedAdapter($memcached);
    $reader = new StreamReader($memcachedAdapter);
    $offerCollection = $reader->read();

    $offerFilterFactory = new OfferFilterFactory();
    $consoleFilter = $offerFilterFactory->createFilter($argv);
    $consoleFilterIterator = new OfferCollectionFilterIterator($offerCollection->getIterator(), $consoleFilter);
    $consoleFilterIterator->addFilter(new OfferInStockFilter());
    foreach ($consoleFilterIterator as $offer) {
        $outputAdapter->output($offer);
    }
} catch (\Exception $ex) {
    $outputAdapter->output($ex);
}