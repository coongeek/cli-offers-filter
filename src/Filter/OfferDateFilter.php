<?php
declare(strict_types=1);

namespace App\Filter;

use App\Entity\OfferInterface;

class OfferDateFilter implements OfferFilterInterface
{
    /** @var \DateTime */
    private $startDate;
    /** @var \DateTime */
    private $endDate;

    public function __construct(\DateTime $startDate, \DateTime $endDate)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    /**
     * @return \DateTime
     */
    public function getStartDate(): \DateTime
    {
        return $this->startDate;
    }

    /**
     * @return \DateTime
     */
    public function getEndDate(): \DateTime
    {
        return $this->endDate;
    }

    public function filter(OfferInterface $offer): bool
    {
        return $offer->getDate() >= $this->getStartDate() && $offer->getDate() <= $this->getEndDate();
    }
}