<?php


namespace App\Filter;


use App\Entity\OfferInterface;

interface OfferFilterInterface
{
    public function filter(OfferInterface $offer): bool;
}