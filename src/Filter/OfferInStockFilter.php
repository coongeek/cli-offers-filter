<?php
declare(strict_types=1);

namespace App\Filter;

use App\Entity\OfferInterface;

class OfferInStockFilter implements OfferFilterInterface
{
    public function filter(OfferInterface $offer): bool
    {
        return $offer->getQuantity() >= 1;
    }
}