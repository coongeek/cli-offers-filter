<?php
declare(strict_types=1);

namespace App\Filter;

use App\Entity\OfferInterface;

class OfferVendorFilter implements OfferFilterInterface
{
    private $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function filter(OfferInterface $offer): bool
    {
        $vendor = $offer->getVendor();

        return $vendor->getId() === $this->getId();
    }
}