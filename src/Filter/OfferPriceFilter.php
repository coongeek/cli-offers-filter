<?php
declare(strict_types=1);

namespace App\Filter;

use App\Entity\OfferInterface;

class OfferPriceFilter implements OfferFilterInterface
{
    private $priceFrom;
    private $priceTo;

    public function __construct(string $priceFrom, string $priceTo)
    {
        $this->validatePriceString($priceFrom);
        $this->validatePriceString($priceTo);

        $this->priceFrom = $priceFrom;
        $this->priceTo = $priceTo;
    }

    public function getPriceFrom(): string
    {
        return $this->priceFrom;
    }

    public function getPriceTo(): string
    {
        return $this->priceTo;
    }

    private function validatePriceString(string $price): void
    {
        $res = preg_match('/^[0-9]+(?:\.[0-9]+)?$/', $price);

        if ($res !== 1) {
            throw new \Exception('Invalid price value: ' . $price);
        }
    }

    public function filter(OfferInterface $offer): bool
    {
        return in_array(bccomp($offer->getPrice(), $this->getPriceFrom(), 2), [0, 1])
            && in_array(bccomp($offer->getPrice(), $this->getPriceTo(), 2), [-1, 0]);
    }
}