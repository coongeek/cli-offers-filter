<?php
declare(strict_types=1);
namespace App\Iterator\FilterIterator;

use App\Filter\OfferFilterInterface;
use App\Iterator\OfferCollectionIterator;
use FilterIterator;

class OfferCollectionFilterIterator extends FilterIterator
{
    /** @var OfferFilterInterface[] */
    private $filters = [];

    public function __construct(OfferCollectionIterator $iterator, OfferFilterInterface $filter)
    {
        $this->filters[] = $filter;
        parent::__construct($iterator);
    }

    public function addFilter(OfferFilterInterface $filter): void
    {
        $this->filters[] = $filter;
    }

    public function accept()
    {
        $offer = $this->getInnerIterator()->current();

        foreach ($this->filters as $filter) {
            if ($filter->filter($offer) === false) {
                return false;
            }
        }

        return true;
    }
}