<?php
declare(strict_types=1);

namespace App\Iterator;

use App\Collection\OfferCollectionInterface;
use Iterator;

class OfferCollectionIterator implements Iterator
{
    private $offerCollection;
    private $position = 0;

    public function __construct(OfferCollectionInterface $offerCollection)
    {
        $this->offerCollection = $offerCollection;
    }

    public function rewind()
    {
        $this->position = 0;
    }

    public function current()
    {
        return $this->offerCollection->get($this->position);
    }

    public function key()
    {
        return $this->position;
    }

    public function next()
    {
        $this->position = $this->position + 1;
    }

    public function valid()
    {
        return $this->offerCollection->isValid($this->position);
    }
}