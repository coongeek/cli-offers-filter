<?php
declare(strict_types=1);

namespace App\Collection;

use App\Entity\OfferInterface;
use App\Iterator\OfferCollectionIterator;
use Iterator;

class OffferCollection implements OfferCollectionInterface
{
    protected $data = [];

    public function add(OfferInterface $offer): void
    {
        $this->data[] = $offer;
    }

    public function get(int $key): OfferInterface
    {
        return $this->data[$key];
    }

    public function getIterator(): Iterator
    {
        return new OfferCollectionIterator($this);
    }

    public function count()
    {
        return $this->count($this->data);
    }

    public function isValid(int $key): bool
    {
        return isset($this->data[$key]);
    }
}