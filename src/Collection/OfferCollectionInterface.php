<?php

namespace App\Collection;

use App\Entity\OfferInterface;
use Iterator;
use Countable;

interface OfferCollectionInterface extends Countable
{
    public function add(OfferInterface $offer): void;
    public function get(int $key): OfferInterface;
    public function isValid(int $key): bool;
    public function getIterator(): Iterator;
}