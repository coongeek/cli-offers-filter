<?php

namespace App\Model\Adapter;

interface OutputAdapterInterface
{
    public function output($data);
}