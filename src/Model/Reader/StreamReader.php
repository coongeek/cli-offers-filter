<?php

namespace App\Model\Reader;

use App\Collection\OffferCollection;
use App\Collection\OfferCollectionInterface;
use App\Entity\Offer;
use App\Entity\OfferInterface;
use DateTime;
use Doctrine\Common\Annotations\AnnotationReader;
use Psr\Cache\CacheItemPoolInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

class StreamReader implements ReaderInterface
{
    const CACHE__COLLECTION_DEFAULT_POSITION = 0;
    /**
     * Size of cached collection
     * @var string
     */
    const CACHE_KEY__OFFER_AMOUNT = "offer.amount";
    /**
     * @var CacheItemPoolInterface|null
     */
    protected $cache;
    protected $cachedKeys = [];

    public function __construct(CacheItemPoolInterface $cacheItemPool = null)
    {
        $this->cache = $cacheItemPool;
    }

    public function read(): OfferCollectionInterface
    {
        $offerCollection = $this->readFromCache();
        if (! is_null($offerCollection)) {
            return $offerCollection;
        }

        $serializer = $this->makeSerializer();
        $offerCollection = new OffferCollection();
        $previousChunkContentLeftovers = '';
        $offerPosition = 0;

        $now = new DateTime();
        $cacheExpirationTime = $now->add(new \DateInterval('PT2M'));

        $httpClient = HttpClient::create();
        $response = $httpClient->request('GET', 'http://localhost:8000/offers/lazy');

        if ($response->getStatusCode() !== 200) {
            throw new \Exception('Fail');
        }

        try {
            foreach ($httpClient->stream($response) as $chunk) {
                $content = $previousChunkContentLeftovers . $chunk->getContent();
                $previousChunkContentLeftovers = '';
                $linesSeparator = PHP_EOL;
                $lines = preg_split("/" . $linesSeparator . "/", $content);//what if separator would be found in line
                $lastLinePosition = count($lines);
                $currentLinePosition = 0;

                foreach ($lines as $line) {
                    if (empty($line)) {
                        continue; //TODO - it's just our assumption, need to handle it more graceful
                    }

                    $currentLinePosition++;

                    if ($currentLinePosition === $lastLinePosition) {
                        $previousChunkContentLeftovers = $line;
                        break;
                    }

                    $line = $this->unwrapFromArrayBrackets($line);
                    $offer = $serializer->deserialize($line, Offer::class, 'json');
                    $offerCollection->add($offer);
                    $this->storeInCache($offer, $offerPosition, $cacheExpirationTime);
                    $offerPosition++;
                }
            }

            return $offerCollection;
        } catch(\Exception $ex) {
            $this->clearCache();
            throw $ex;
        }
    }

    private function unwrapFromArrayBrackets(string $item): string
    {
        return mb_substr($item, 1, mb_strlen($item) - 2);
    }

    protected function makeSerializer(): SerializerInterface
    {
        $encoders = [new JsonEncoder()];
        $normalizers = [
            new DateTimeNormalizer(['Y-m-dTH:i:sP']),
            new ObjectNormalizer(null, null, null, new ReflectionExtractor()),
        ];
        $serializer = new Serializer($normalizers, $encoders);

        return $serializer;
    }

    protected function getCacheKey(int $collectionPosition): string
    {
        return 'offer.' . $collectionPosition;
    }

    protected function readFromCache(): ?OfferCollectionInterface
    {
        if (is_null($this->cache)) {
            return null;
        }

        $offferAmount = $this->cache->getItem(self::CACHE_KEY__OFFER_AMOUNT);

        if (! $offferAmount->isHit()) {
            return null;
        }

        $offferAmount = $offferAmount->get();
        $i = self::CACHE__COLLECTION_DEFAULT_POSITION;
        $offerCollection = new OffferCollection();

        while ($i < $offferAmount) {
            $cacheKey = $this->getCacheKey($i);
            $offer = $this->cache->getItem($cacheKey);

            if (! $offer->isHit()) {
                break;
            }

            $offer = $offer->get();
            $offerCollection->add($offer);
            $i++;
        }

        if ($i !== $offferAmount) {
            return null;
        }

        return $offerCollection;
    }

    protected function storeInCache(OfferInterface $offer, int $collectionPosition, DateTime $expiresAt): bool
    {
        if (is_null($this->cache)) {
            return false;
        }

        $cacheKey = $this->getCacheKey($collectionPosition) ;
        $cachedOffer = $this->cache->getItem($cacheKey);
        if ($cachedOffer->isHit()) {
            throw new \LogicException();
        }

        //TODO: what if we will get an error while reading.. - we will end up with messed up collection?
        $cachedOffer->set($offer);
        $cachedOffer->expiresAt($expiresAt);
        $isCached = $this->cache->save($cachedOffer);

        if (!$isCached) {
            return false;
        }

        $this->cachedKeys[] = $cacheKey;

        $offerAmountItem = $this->cache->getItem(self::CACHE_KEY__OFFER_AMOUNT);
        $offerAmount = (int)$offerAmountItem->get();
        $offerAmount++;
        $offerAmountItem->set($offerAmount);
        $offerAmountItem->expiresAt($expiresAt);

        $isCached = $this->cache->save($offerAmountItem);
        if (!$isCached) {
            return false;
        }

        return true;
    }

    protected function clearCache(): bool
    {
        if (is_null($this->cache)) {
            return false;
        }

        foreach ($this->cachedKeys as $cachedKey) {
            if ($this->cache->hasItem($cachedKey)) {
                $this->cache->deleteItem($cachedKey);
            }
        }

        if ($this->cache->hasItem(self::CACHE_KEY__OFFER_AMOUNT)) {
            $this->cache->deleteItem(self::CACHE_KEY__OFFER_AMOUNT);
        }

        return true;
    }
}