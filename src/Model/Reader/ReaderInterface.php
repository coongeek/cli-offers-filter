<?php


namespace App\Model\Reader;

use App\Collection\OfferCollectionInterface;

interface ReaderInterface
{
    public function read(): OfferCollectionInterface;
}