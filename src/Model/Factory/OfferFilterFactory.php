<?php
declare(strict_types=1);

namespace App\Model\Factory;

use App\Filter\OfferDateFilter;
use App\Filter\OfferFilterInterface;
use App\Filter\OfferPriceFilter;
use App\Filter\OfferVendorFilter;
use Exception;
use DateTime;

class OfferFilterFactory
{
    const FILTER_NAME__DATE_FILTER = 'date_filter';
    const FILTER_NAME__PRICE_FILTER = 'price_filter';
    const FILTER_NAME__VENDOR_OFFERS_FILTER = 'vendor_offers_count';

    public function createFilter(array $options): OfferFilterInterface
    {
        switch ($options[1]) {
            case self::FILTER_NAME__DATE_FILTER:
                $dateStart = DateTime::createFromFormat('Y-m-d\TH:i', $options[2]);
                $dateEnd = DateTime::createFromFormat('Y-m-d\TH:i', $options[3]);
                $filter = new OfferDateFilter($dateStart, $dateEnd);
                break;
            case self::FILTER_NAME__PRICE_FILTER:
                $filter = new OfferPriceFilter($options[2], $options[3]);
                break;
            case self::FILTER_NAME__VENDOR_OFFERS_FILTER:
                $filter = new OfferVendorFilter((int)$options[2]);
                break;
            default:
                throw new Exception('Filter type is not supported.');
        }

        return $filter;
    }
}