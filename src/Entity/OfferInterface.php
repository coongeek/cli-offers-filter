<?php

namespace App\Entity;

use DateTime;

interface OfferInterface
{
    public function getId(): int;
    public function getName(): string;
    public function getDate(): DateTime;
    public function getPrice(): string;
    public function getVendor(): VendorInterface;
    public function getQuantity(): int;
}