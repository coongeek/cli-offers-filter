<?php
declare(strict_types=1);

namespace App\Entity;

use DateTime;

class Offer implements OfferInterface
{
    /** @var int */
    private $id;
    /** @var string */
    private $name;
    /** @var DateTime */
    private $date;
    /** @var string */
    private $price;
    /** @var int */
    private $quantity;
    /** @var VendorInterface */
    private $vendor;

    public function getDate(): DateTime
    {
        return $this->date;
    }

    public function setDate(DateTime $date): Offer
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return string
     */
    public function getPrice(): string
    {
        return $this->price;
    }

    /**
     * @param string $price
     * @return Offer
     */
    public function setPrice(string $price): Offer
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return VendorInterface
     */
    public function getVendor(): VendorInterface
    {
        return $this->vendor;
    }

    /**
     * @param VendorInterface $vendor
     * @return Offer
     */
    public function setVendor(Vendor $vendor): Offer
    {
        $this->vendor = $vendor;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Offer
     */
    public function setId(int $id): Offer
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Offer
     */
    public function setName(string $name): Offer
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        if (is_null($this->quantity)) {
            //TODO: remove
            $this->quantity = rand(0, 2);
        }

        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return Offer
     */
    public function setQuantity(int $quantity): Offer
    {
        $this->quantity = $quantity;
        return $this;
    }
}