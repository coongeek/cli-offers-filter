<?php

namespace App\Entity;


interface VendorInterface
{
    public function getId(): int;
    public function getName(): string;
}