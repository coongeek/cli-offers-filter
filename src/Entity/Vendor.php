<?php
declare(strict_types=1);

namespace App\Entity;

class Vendor implements VendorInterface
{
    /** @var int */
    private $id;
    /** @var string */
    private $name;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Vendor
     */
    public function setId(int $id): Vendor
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Vendor
     */
    public function setName(string $name): Vendor
    {
        $this->name = $name;
        return $this;
    }
}